package com.mitocode.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitocode.dto.CambiarClave;
import com.mitocode.model.Cliente;
import com.mitocode.model.Usuario;
import com.mitocode.repo.IUsuarioRepo;
import com.mitocode.service.IUsuarioService;

@Service
public class UsuarioServiceImpl implements UserDetailsService ,IUsuarioService {
	
	@Autowired
	private IUsuarioRepo userRepo;
	
	@Value("${mitocine.default-rol}")
	private Integer DEFAULT_ROL;
	
	@Autowired
	PasswordEncoder password_encoder; 
	
	@Autowired
	private BCryptPasswordEncoder bcrypt;
	
	@Transactional
	@Override
	public Usuario registrarTransaccional(Usuario usuario) {
		Usuario u;
		try {
			usuario.setClave(bcrypt.encode(usuario.getClave()));
			u = userRepo.save(usuario);
			userRepo.registrarRolPorDefecto(u.getIdUsuario(), DEFAULT_ROL);
			if (u.getCliente().getFoto()!=null) {
				userRepo.modificarFoto(u.getIdUsuario(), u.getCliente().getFoto());
			}
		}catch(Exception e) {
			throw e;
		}		
		return u;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario user = userRepo.findOneByNombre(username); //from usuario where nombre := username
		
		if (user == null) {
			throw new UsernameNotFoundException(String.format("Usuario no existe", username));
		}
		
		List<GrantedAuthority> roles = new ArrayList<>();
		
		user.getRoles().forEach( role -> {
			roles.add(new SimpleGrantedAuthority(role.getNombre()));
		});
		
		UserDetails userDetails = new User(user.getNombre(), user.getClave(), roles);
		
		return userDetails;
	}

	@Override
	public Usuario listarPorNombre(String nombreUsuario) {
		
		return userRepo.findOneByNombre(nombreUsuario);
	}

	@Override
	public Usuario listarPorId(Integer id) {
		Optional<Usuario> user=userRepo.findById(id);
		return user.isPresent()?user.get():new Usuario();
	}
	
	@Transactional
	@Override
	public Usuario Modificar(Usuario user) {
		System.out.println(user);
		if (user.getClave().equals("")) {
			user.setClave(user.getClaveoculta());
		}else {
			user.setClave(bcrypt.encode(user.getClave()));
		}
		if (user.getCliente().getFoto().length>0) {
			userRepo.modificarFoto(user.getIdUsuario(), user.getCliente().getFoto());
		}
		Usuario usuario=userRepo.save(user);
		return usuario;
	}

	@Override
	public Boolean checkeaValidezPassword(Usuario user, String passwordAntigua) {
		return password_encoder.matches(passwordAntigua, user.getClave());
	}

	@Override
	public Usuario CambiarClave(Usuario user, String nueva_clave) {
		user.setClave(password_encoder.encode(nueva_clave));
		userRepo.actualizaContraseña(user.getIdUsuario(), user.getClave(), true);
		userRepo.registrarRolPorDefecto(user.getIdUsuario(), DEFAULT_ROL);
		return listarPorNombre(user.getNombre());
	}
	@Transactional
	@Override
	public Usuario registrar(Usuario user) {
		user.setClave(bcrypt.encode(user.getClave()));
		if (user.getCliente().getFoto().length>0) {
			userRepo.modificarFoto(user.getIdUsuario(), user.getCliente().getFoto());
		}
		return userRepo.save(user);
	}
}
