package com.mitocode;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.mitocode.model.Cliente;
import com.mitocode.model.Usuario;
import com.mitocode.service.IClienteService;
import com.mitocode.service.IUsuarioService;
import com.mitocode.service.impl.UsuarioServiceImpl;

import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
class CineappBackendApplicationTests {

	@Autowired
	private IUsuarioService service;
	
	
	@Autowired
	private UsuarioServiceImpl service_impl;
	
	private IClienteService cliente_service;
	
	@Autowired
	private BCryptPasswordEncoder bcrypt;

	
	@Test
	public void crearUsuario() {
		Usuario us = new Usuario();
		us.setIdUsuario(6);
		us.setNombre("user");
		us.setClave(bcrypt.encode("1234"));//51st3ma$
		us.setEstado(true);

		Cliente c = new Cliente();
		c.setIdCliente(6);
		/*
		c.setNombres("JOSE TEST");
		c.setApellidos("CASTILLO CHALQUE");
		c.setDni("12345646");
		c.setFechaNac(LocalDate.of(1996, 3, 12));
		*/
		c.setUsuario(us);
		us.setCliente(c);
		
		System.out.println(bcrypt.encode("1234"));
		//service.registrarTransaccional(us);
		
	}

}
