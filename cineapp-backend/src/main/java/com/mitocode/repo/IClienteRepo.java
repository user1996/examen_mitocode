package com.mitocode.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mitocode.model.Cliente;

public interface IClienteRepo extends JpaRepository<Cliente, Integer>{
	@Query(value = "SELECT * FROM cliente c ORDER BY c.id_cliente",nativeQuery = true)
	List<Cliente> findAllOrderByIdClienteAsc();
}
