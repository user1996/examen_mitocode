package com.mitocode.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.model.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, Integer> {
				
	Usuario findOneByNombreAndEstado(String username,Boolean estado);
	
	@Modifying
	@Query(value = "INSERT INTO usuario_rol (id_usuario, id_rol) VALUES (:idUsuario, :idRol)", nativeQuery = true)
	void registrarRolPorDefecto(@Param("idUsuario") Integer idUsuario, @Param("idRol") Integer idRol);
	
	
	@Modifying
	@Query(value ="UPDATE usuario u SET u.nombre=:nombre,u.estado=:estado WHERE u.id_usuario=:idUsuario",nativeQuery = true)
	void actualizaDatos(@Param("idUsuario") Integer idUsuario,@Param("nombre") String nombre,@Param("estado") Boolean estado);
	
	@Modifying
	@Query(value ="UPDATE usuario u SET u.clave=:clave,u.estado=:estado WHERE u.id_usuario=:idUsuario",nativeQuery = true)
	void actualizaContraseña(@Param("idUsuario") Integer idUsuario,@Param("clave") String clave,@Param("estado") Boolean estado);
	
	@Modifying
	@Query("update Cliente set foto=:foto where id=:id")
	void modificarFoto(@Param("id")Integer id,@Param("foto") byte[]foto);
}