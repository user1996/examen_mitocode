package com.mitocode.dto;

public class CambiarClave {
	
	private String _nombreusuario;
	
	private String _actualcontraseña;
	
	private String _nuevacontraseña;
	
	public String get_nombreusuario() {
		return _nombreusuario;
	}

	public void set_nombreusuario(String _nombreusuario) {
		this._nombreusuario = _nombreusuario;
	}

	public String get_actualcontraseña() {
		return _actualcontraseña;
	}

	public void set_actualcontraseña(String _actualcontraseña) {
		this._actualcontraseña = _actualcontraseña;
	}

	public String get_nuevacontraseña() {
		return _nuevacontraseña;
	}

	public void set_nuevacontraseña(String _nuevacontraseña) {
		this._nuevacontraseña = _nuevacontraseña;
	}

	@Override
	public String toString() {
		return "CambiarClave [_nombreusuario=" + _nombreusuario + ", _actualcontraseña=" + _actualcontraseña
				+ ", _nuevacontraseña=" + _nuevacontraseña + "]";
	}
}
