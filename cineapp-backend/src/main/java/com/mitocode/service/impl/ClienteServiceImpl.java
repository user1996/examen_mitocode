package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.model.Cliente;
import com.mitocode.model.Genero;
import com.mitocode.repo.IClienteRepo;
import com.mitocode.service.IClienteService;

@Service
public class ClienteServiceImpl implements IClienteService{

	@Autowired
	private IClienteRepo repo;
	
	@Override
	public Cliente registrar(Cliente obj) {
		return repo.save(obj);
	}

	@Transactional
	@Override
	public Cliente modificar(Cliente obj) {

		return repo.save(obj);
	}
	
	@Override
	public List<Cliente> listar() {
		return repo.findAllOrderByIdClienteAsc();
	}

	@Override
	public Cliente listarPorId(Integer v) { 
		Optional<Cliente> op = repo.findById(v);
		return op.isPresent() ? op.get() : new Cliente();
	}

	@Override
	public void eliminar(Integer v) {
		repo.deleteById(v);
	}
	@Override
	public Page<Cliente> listarPageable(Pageable pageable) {
		return repo.findAll(pageable);
	}
}
