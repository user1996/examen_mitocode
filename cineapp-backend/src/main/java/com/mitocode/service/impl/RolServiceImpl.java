package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.model.Rol;
import com.mitocode.repo.IRolRepo;
import com.mitocode.service.IRolService;

@Service
public class RolServiceImpl implements IRolService{

	@Autowired
	IRolRepo repo;
	
	@Override
	public Rol registrar(Rol obj) {
		
		return repo.save(obj);
	}

	@Override
	public Rol modificar(Rol obj) {
		
		return repo.save(obj);
	}

	@Override
	public List<Rol> listar() {
		
		return repo.findAll();
	}

	@Override
	public Rol listarPorId(Integer v) {
		Optional<Rol> rol=repo.findById(v);
		return rol.isPresent()?rol.get():new Rol();
	}

	@Override
	public void eliminar(Integer v) {
		repo.deleteById(v);
	}

	@Override
	public Page<Rol> listarPageable(Pageable pageable) {
		
		return repo.findAll(pageable);
	}

}
