package com.mitocode.controller;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Cliente;
import com.mitocode.model.Comida;
import com.mitocode.model.Genero;
import com.mitocode.model.Usuario;
import com.mitocode.service.IClienteService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@RequestMapping("/clientes")
public class ClienteController {
	
	@Autowired
	private IClienteService service;
	
	@GetMapping(value="/pageable", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<Cliente>> listarPageable(Pageable pageable){
		Page<Cliente> clientes;
		clientes = service.listarPageable(pageable);
		return new ResponseEntity<Page<Cliente>>(clientes, HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<List<Cliente>> listar(){
		List<Cliente> lista = service.listar();
		return new ResponseEntity<List<Cliente>>(lista, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Cliente> listarPorId(@PathVariable("id") Integer id) {
		Cliente cli = service.listarPorId(id);
		if(cli.getIdCliente() == null) {
			throw new ModeloNotFoundException("ID NO EXISTE: " + id);
		}
		return new ResponseEntity<Cliente>(cli, HttpStatus.OK);
	}
	@GetMapping(value = "/cliente/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> listarPor_Id(@PathVariable("id") Integer id) {
		Cliente c = service.listarPorId(id);
		byte[] data = c.getFoto();
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	//Spring Boot 2.1 | Hateoas 0.9
		/*@GetMapping(value = "/{id}")
		public Resource<Cliente> listarPorId(@PathVariable("id") Integer id){
			
			Cliente pel = service.listarPorId(id);
			if(pel.getIdCliente() == null) {
				throw new ModeloNotFoundException("ID NO ENCONTRADO : " + id);
			}
			
			Resource<Cliente> resource = new Resource<Cliente>(pel);
			// /Clientes/{4}
			ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(id));
			resource.add(linkTo.withRel("Cliente-resource"));
			
			return resource;
		}*/
	
	@GetMapping("/hateoas/{id}")
	//Spring Boot 2.2 | Hateoas 1
	public EntityModel<Cliente> listarPorIdHateoas(@PathVariable("id") Integer id){
		Cliente pel = service.listarPorId(id);
		if(pel.getIdCliente() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO : " + id);
		}
		EntityModel<Cliente> resource = new EntityModel<Cliente>(pel);
		// /Clientes/{4}
		WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(id));
		resource.add(linkTo.withRel("Cliente-resource"));
		
		return resource;
	}
	
	/*@PostMapping
	private ResponseEntity<Cliente> registrar(@Valid @RequestBody Cliente obj) {
		Cliente pel = service.registrar(obj);
		return new ResponseEntity<Cliente>(pel, HttpStatus.CREATED);
	}*/
	
	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Cliente obj) {
		Cliente pel = service.registrar(obj);
		
		// localhost:8080/Clientes/2
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pel.getIdCliente()).toUri();
		return ResponseEntity.created(location).build();
	}
	///////////////
	@PostMapping("/saveCliente")
	public Cliente registraCliente(@RequestPart("cliente") Cliente cliente, @RequestPart("file") MultipartFile file) throws IOException {
		Cliente c=cliente;
		c.setFoto(file.getBytes());
		return service.registrar(c);
	}
	@PutMapping("/updateCliente")
	public Cliente actualziaCliente(@RequestPart("cliente") Cliente cliente, @RequestPart("file") MultipartFile file) throws IOException {
		Cliente c=cliente;
		c.setFoto(file.getBytes());
		return service.modificar(c);
	}
	///////////////////
	@PutMapping
	public ResponseEntity<Cliente>  modificar(@Valid @RequestBody Cliente obj) {
		Cliente pel = service.modificar(obj);
		return new ResponseEntity<Cliente>(pel, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
		Cliente pel = service.listarPorId(id);
		if(pel.getIdCliente() == null) {
			throw new ModeloNotFoundException("ID NO EXISTE: " + id);
		}
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}

}
