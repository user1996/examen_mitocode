package com.mitocode.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mitocode.dto.CambiarClave;
import com.mitocode.exception.InvalidOldPasswordException;
import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Comida;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private IUsuarioService service;
	
	@Autowired
	private BCryptPasswordEncoder bcrypt;
		
	@PostMapping("/save")
	private ResponseEntity<Object> registrar(@RequestBody Usuario usuario){
		System.out.println(usuario);
		usuario.setClave(bcrypt.encode(usuario.getClave()));
		service.registrarTransaccional(usuario);
		return new ResponseEntity<Object>(HttpStatus.CREATED);
	}
	@GetMapping("/{id}")
	public ResponseEntity<Usuario> listarPorId(@PathVariable("id") Integer id) {
		Usuario user = service.listarPorId(id);
		if(user.getIdUsuario() == null) {
			throw new ModeloNotFoundException("ID NO EXISTE: " + id);
		}
		return new ResponseEntity<Usuario>(user, HttpStatus.OK);
	}
	@GetMapping(value = "/findBy/{nombre}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> listarPorNombre(@PathVariable("nombre") String nombre) {
		Usuario c = service.listarPorNombre(nombre);
		byte[] data = c.getCliente().getFoto();
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	@PostMapping("/saveUsuario")
	public Usuario registrar_file(@RequestPart("usuario") Usuario usuario, @RequestPart("file") MultipartFile file)
			throws IOException {
		Usuario user = usuario;
		user.getCliente().setFoto(file.getBytes());		
		return service.registrar(user);
	}

	@PutMapping("/updateUsuario")
	public Usuario modificar_file(@RequestPart("usuario") Usuario usuario, @RequestPart("file") MultipartFile file)
			throws IOException {
		Usuario user = usuario;
		user.getCliente().setFoto(file.getBytes());
		return service.Modificar(user);
	}
	@PutMapping("/changePassword")
	public Usuario cambiar_passord(@RequestBody CambiarClave usuario){
		Usuario user=service.listarPorNombre(usuario.get_nombreusuario());
		if (!service.checkeaValidezPassword(user, usuario.get_actualcontraseña())) {
			throw new InvalidOldPasswordException("LA CONTRASEÑA ACTUAL ES INCORRECTA");
		}
		return service.CambiarClave(user, usuario.get_nuevacontraseña());
	}
}
