package com.mitocode.exception;

public class InvalidOldPasswordException extends RuntimeException {

	private static final long serialVersionUID = -6198038858276421285L;
	
	public InvalidOldPasswordException() {
        super();
    }

    public InvalidOldPasswordException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidOldPasswordException(final String message) {
        super(message);
    }

    public InvalidOldPasswordException(final Throwable cause) {
        super(cause);
    }
}
