import { Subject } from 'rxjs';
import { Cliente } from './../_model/cliente';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  
  clienteCambio = new Subject<Cliente[]>();
  mensajeCambio = new Subject<string>();
  url: string = `${environment.HOST}/clientes`;    
  //url: string = `${environment.HOST}/${environment.MICRO_CRUD}/clientes`;   

  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get<Cliente[]>(this.url);
  }
  listarPorId(id: number) {
    return this.http.get<Cliente>(`${this.url}/${id}`);
  }
  getFotoPorId(id: number) {
    return this.http.get(`${this.url}/cliente/${id}`, {
      responseType: 'blob'
    });
  }
  eliminar(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }
  listarPageable(p: number, s: number) {
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`); //&sort=nombre
  }
}
