import { CambiarClave } from './../_model/cambiarClaveDTO';
import { Subject } from 'rxjs';
import { Usuario } from 'src/app/_model/usuario';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  usuarioCambio = new Subject<Usuario[]>();
  mensajeCambio = new Subject<string>();
  url: string = `${environment.HOST}/usuarios`;  
  //url: string = `${environment.HOST}/${environment.MICRO_CRUD}/usuarios`;  

  constructor(private http: HttpClient) { }

  modificarContraseña(change: CambiarClave) {
    return this.http.put(this.url+'/changePassword', change);
  }

  registrar(usuario: Usuario) {
    return this.http.post(`${this.url}/save`, usuario);
  }
  listarPorId(id:number){
    return this.http.get<Usuario>(`${this.url}/${id}`);
  }
  listarPorNombre(nombre: string) {
    return this.http.get(`${this.url}/findBy/${nombre}`, {
      responseType: 'blob'
    });
  }
  registrarConFoto(usuario: Usuario, file?: File) {
    let formdata: FormData = new FormData();
    formdata.append('file', file);

    const usuarioBlob = new Blob([JSON.stringify(usuario)], { type: "application/json" });
    formdata.append('usuario', usuarioBlob);

    return this.http.post(`${this.url+'/saveUsuario'}`, formdata);
  }

  modificarConFoto(usuario: Usuario, file?: File) {
    let formdata: FormData = new FormData();
    formdata.append('file', file);

    const usuarioBlob = new Blob([JSON.stringify(usuario)], { type: "application/json" });
    formdata.append('usuario', usuarioBlob);

    return this.http.put(`${this.url+'/updateUsuario'}`, formdata);
  }
}
