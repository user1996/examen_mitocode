import { Rol } from './../../../_model/rol';
import { RolService } from 'src/app/_service/rol.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Cliente } from 'src/app/_model/cliente';
import { PasswordValidation } from './match';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { Usuario } from 'src/app/_model/usuario';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css']
})
export class NuevoComponent implements OnInit {

  form: FormGroup;
  maxFecha: Date;
  roles:Rol[];
  idRolSeleccionado:number;
  constructor(private fb: FormBuilder,
    private router: Router,
    private usuarioService: UsuarioService,
    private matSnackBar: MatSnackBar,
    private rolService:RolService
    ) {
  }

  ngOnInit() {
    this.maxFecha = new Date();
    this.form = this.fb.group({
      'nombres': new FormControl('',Validators.required),
      'apellidos': new FormControl('',Validators.required),
      'dni': new FormControl('',Validators.required),
      'fechaNac': new Date(),
      usuario: new FormControl('',Validators.required),
      roles:['',Validators.required],
      password: [''],
      confirmPassword: ['']
    }, {
        validator: PasswordValidation.MatchPassword
      });
      this.listarRoles();
      this.idRolSeleccionado=0;
  }

  registrar() {
    let cliente = new Cliente();
    cliente.nombres = this.form.value['nombres'];
    cliente.apellidos = this.form.value['apellidos'];
    cliente.dni = this.form.value['dni'];
    cliente.fechaNac = this.form.value['fechaNac'];

    let usuario = new Usuario();
    usuario.nombre = this.form.value['usuario'];
    usuario.clave = this.form.value['password'];
    usuario.estado = true;
    usuario.cliente = cliente;
    console.log(usuario);
    this.usuarioService.registrar(usuario).subscribe(() => {
      this.matSnackBar.open('Se creó usuario', 'INFO', {
        duration: 2000
      });

      setTimeout(() => {
        this.router.navigate(['login']);
      }, 1500);
    });
  }
listarRoles(){
  this.rolService.listar().subscribe(data=>{
    this.roles=data;
  })
}
}
