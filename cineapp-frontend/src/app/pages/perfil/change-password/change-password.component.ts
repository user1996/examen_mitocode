import { LoginService } from "./../../../_service/login.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { CambiarClave } from "./../../../_model/cambiarClaveDTO";
import { UsuarioService } from "src/app/_service/usuario.service";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { JwtHelperService } from "@auth0/angular-jwt";
import { environment } from "src/environments/environment";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.css"],
})
export class ChangePasswordComponent implements OnInit {
  user_name: string;
  actual_password: string;
  nueva_password: string;
  form: FormGroup;
  cambio_clave: CambiarClave;
  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private usuario_service: UsuarioService,
    private login_service: LoginService
  ) {}

  ngOnInit(): void {
    let token = sessionStorage.getItem(environment.TOKEN_NAME);
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    this.user_name = decodedToken.user_name;
    this.init_form();
    this.usuario_service.mensajeCambio.subscribe((data) => {
      this.snackBar.open(data, "INFO", {
        duration: 2000,
      });
    });
  }
  init_form() {
    this.form = this.fb.group({
      user_name: new FormControl(this.user_name),
      actual_clave: new FormControl(),
      nueva_clave: new FormControl(),
    });
  }
  registrar() {
    this.cambio_clave = new CambiarClave();
    this.cambio_clave._nombreusuario = this.form.value["user_name"];
    this.cambio_clave._actualcontraseña = this.form.value["actual_clave"];
    this.cambio_clave._nuevacontraseña = this.form.value["nueva_clave"];
    this.usuario_service
      .modificarContraseña(this.cambio_clave)
      .subscribe(() => {
        this.usuario_service.mensajeCambio.next("Se cambio la clave");
        this.login_service.cerrarSesion();
      });
  }
}
