import { DomSanitizer } from "@angular/platform-browser";
import { UsuarioService } from "src/app/_service/usuario.service";
import { JwtHelperService } from "@auth0/angular-jwt";
import { environment } from "./../../../environments/environment.prod";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-perfil",
  templateUrl: "./perfil.component.html",
  styleUrls: ["./perfil.component.css"]
})
export class PerfilComponent implements OnInit {
  user_name: string;
  roles = [];
  imagenData: any;
  imagenEstado: boolean = false;
  style:any;
  
  constructor(
    private usuario_service: UsuarioService,
    private sanitization: DomSanitizer
  ) {}

  ngOnInit(): void {
    let token = sessionStorage.getItem(environment.TOKEN_NAME);
    const helper = new JwtHelperService();
      //SI TIENES EL ROL NECESARIO
      const decodedToken = helper.decodeToken(token);
      this.user_name = decodedToken.user_name;
      this.roles = decodedToken.authorities;
      this.usuario_service.listarPorNombre(this.user_name).subscribe(data => {
        if (data.size>0){
          this.convertir(data);
        }
      });
  
  }
  convertir(data: any) {
    let reader = new FileReader();
    reader.readAsDataURL(data);
    reader.onloadend = () => {
      let base64 = reader.result;
      this.sanar(base64);
    };
  }
  sanar(base64: any) {
    this.imagenData = this.sanitization.bypassSecurityTrustResourceUrl(base64);
    this.imagenEstado = true;
  }
}
