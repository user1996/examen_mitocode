import { Util } from "./../../../_shared/utils";
import { Rol } from "./../../../_model/rol";
import { RolService } from "./../../../_service/rol.service";
import { PasswordValidation } from "./../../login/nuevo/match";
import { Usuario } from "./../../../_model/usuario";
import { UsuarioService } from "src/app/_service/usuario.service";
import { environment } from "src/environments/environment";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { DomSanitizer } from "@angular/platform-browser";
import { ClienteService } from "./../../../_service/cliente.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Cliente } from "./../../../_model/cliente";
import { Component, OnInit, Inject } from "@angular/core";

@Component({
  selector: "app-cliente-dialog",
  templateUrl: "./cliente-dialog.component.html",
  styleUrls: ["./cliente-dialog.component.css"],
})
export class ClienteDialogComponent implements OnInit {
  message: string;
  cliente: Cliente;
  usuario: Usuario;
  imagenData: any;
  imagenEstado: boolean = false;
  selectedFiles: FileList;
  currentFileUpload: File;
  labelFile: string;
  form: FormGroup;
  edicion: boolean = false;
  maxFecha: Date;
  roles: Rol[];
  idRolSeleccionado: number;
  foto_invalida:boolean=false;
  constructor(
    private dialogRef: MatDialogRef<ClienteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Cliente,
    private fb: FormBuilder,
    private cliente_service: ClienteService,
    private usuario_service: UsuarioService,
    private rol_service: RolService,
    private sanitization: DomSanitizer
  ) {}
  get f() {
    return this.form.controls;
  }

  ngOnInit(): void {
    this.maxFecha = new Date();
    this.init_form(false);
    this.edicion = this.data.idCliente > 0;
    if (this.edicion) {
      this.usuario_service
        .listarPorId(this.data.idCliente)
        .subscribe((data) => {
          this.cliente_service
            .getFotoPorId(this.data.idCliente)
            .subscribe((data) => {
              if (data.size > 0) {
                this.convertir(data);
              }
            });
          this.init_form(true, data);
        });
    }
    this.rol_service.listar().subscribe((data) => {
      this.roles = data;
    });
  }
  init_form(edita: boolean, user?: Usuario) {
    this.form = this.fb.group(
      {
        idCliente: new FormControl(edita ? user.cliente.idCliente : 0),
        dni: new FormControl(edita ? user.cliente.dni : "", [
          Validators.required,
          Validators.pattern(environment.SOLO_NUMEROS),
        ]),
        nombres: new FormControl(edita ? user.cliente.nombres : "", [
          Validators.required,
          Validators.pattern(environment.SOLO_LETRAS),
        ]),
        apellidos: new FormControl(edita ? user.cliente.apellidos : "", [
          Validators.required,
          Validators.pattern(environment.SOLO_LETRAS),
        ]),
        fechaNac: new FormControl(
          edita ? user.cliente.fechaNac : "",
          Validators.required
        ),
        file: new FormControl(""),
        fileSource: new FormControl(""),
        idUsuario: new FormControl(edita ? user.idUsuario : 0),
        nombre: new FormControl(edita ? user.nombre : "", [
          Validators.required,
        ]),
        roles: [edita ? Util.idRol(user.roles) : '', [Validators.required]],
        password: new FormControl("", edita ? null : Validators.required),
        confirmPassword: new FormControl(
          "",
          edita ? null : Validators.required
        ),
        passwordhide: new FormControl(edita ? user.clave : ""),
        estado: new FormControl(
          edita ? user.estado : false,
          edita ? null : Validators.required
        ),
      },
      {
        validator: PasswordValidation.MatchPassword,
      }
    );
    if (edita) {
      this.idRolSeleccionado = Util.idRol(user.roles);
    }
  }
  convertir(data: any) {
    let reader = new FileReader();
    reader.readAsDataURL(data);
    reader.onloadend = () => {
      let base64 = reader.result;
      this.sanar(base64);
    };
  }

  sanar(base64: any) {
    this.imagenData = this.sanitization.bypassSecurityTrustResourceUrl(base64);
    this.imagenEstado = true;
  }
  seleccionarArchivo(e: any) {
    this.labelFile = e.target.files[0].name;
    this.selectedFiles = e.target.files;
    let lista: FileList = this.selectedFiles;
    const file: File = lista[0];
    const reader = new FileReader();
    this.foto_invalida=file.size>environment.MAX_BITES_FILE;
    this.selectedFiles=this.foto_invalida?null:e.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imagenData =this.foto_invalida?null:reader.result;
        this.form.patchValue({
          fileSource: reader.result,
        });
      };
  }
  cancelar() {}
  registrar() {
    if (this.selectedFiles != null) {
      this.currentFileUpload = this.selectedFiles.item(0);
    } else {
      this.currentFileUpload = new File([""], "blanco");
    }
    let cliente = new Cliente();
    cliente.idCliente = this.form.value["idCliente"];
    cliente.nombres = this.form.value["nombres"];
    cliente.apellidos = this.form.value["apellidos"];
    cliente.dni = this.form.value["dni"];
    cliente.fechaNac = this.form.value["fechaNac"];
    let usuario = new Usuario();
    usuario.idUsuario = this.form.value["idUsuario"];
    usuario.nombre = this.form.value["nombre"];
    (usuario.clave = this.form.value["password"]),
      (usuario.claveoculta = this.form.value["passwordhide"]);
    usuario.estado = this.form.value["estado"];
    usuario.roles = Util.getArrayRol(this.roles, this.idRolSeleccionado);
    usuario.cliente = cliente;
    if (this.edicion) {
      this.usuario_service
        .modificarConFoto(usuario, this.currentFileUpload)
        .subscribe(() => {
          this.cliente_service.listar().subscribe((data) => {
            this.cliente_service.clienteCambio.next(data);
            this.cliente_service.mensajeCambio.next("Se modifico");
          });
        });
    } else {
      this.usuario_service
        .registrarConFoto(usuario, this.currentFileUpload)
        .subscribe(() => {
          this.cliente_service.listar().subscribe((data) => {
            this.cliente_service.clienteCambio.next(data);
            this.cliente_service.mensajeCambio.next("Se Agregó!");
          });
        });
    }
    this.dialogRef.close();
  }
}
