import { ClienteDialogComponent } from "./cliente-dialog/cliente-dialog.component";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { Cliente } from "./../../_model/cliente";
import { MatTableDataSource } from "@angular/material/table";
import { ClienteService } from "./../../_service/cliente.service";
import { Component, OnInit, ViewChild } from "@angular/core";

@Component({
  selector: "app-cliente",
  templateUrl: "./cliente.component.html",
  styleUrls: ["./cliente.component.css"],
})
export class ClienteComponent implements OnInit {
  cantidad: number = 0;
  dataSource: MatTableDataSource<Cliente>;
  displayedColumns = ["idCliente", "dni", "datos", "fechaNac", "acciones"];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private cliente_service: ClienteService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.cliente_service.mensajeCambio.subscribe((data) => {
      this.snackBar.open(data, "INFO", {
        duration: 2000,
      });
    });

    this.cliente_service.clienteCambio.subscribe((data) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    this.cliente_service.listarPageable(0, 10).subscribe(data => {
      this.cantidad = data.totalElements;

      this.dataSource = new MatTableDataSource(data.content);
      //this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  openDialog(cliente?: Cliente) {
    let cli = cliente != null ? cliente : new Cliente();
    this.dialog.open(ClienteDialogComponent, {
      data: cli,
    });
  }
  eliminar(cliente: Cliente) {
    this.cliente_service.eliminar(cliente.idCliente).subscribe(() => {
      this.cliente_service.listar().subscribe((data) => {
        this.cliente_service.clienteCambio.next(data);
        this.cliente_service.mensajeCambio.next(
          "Se Elimino a " + cliente.nombres
        );
      });
    });
  }
  filter(x: string) {
    this.dataSource.filter = x.trim().toLowerCase();
  }
  mostrarMas(e : any){
    //console.log(e);
    this.cliente_service.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;

      this.dataSource = new MatTableDataSource(data.content);
      //this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
}
