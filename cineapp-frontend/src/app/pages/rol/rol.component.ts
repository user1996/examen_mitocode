import { switchMap } from "rxjs/operators";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatDialog } from "@angular/material/dialog";
import { Rol } from "./../../_model/rol";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Component, OnInit, ViewChild } from "@angular/core";
import { RolService } from "src/app/_service/rol.service";

@Component({
  selector: "app-rol",
  templateUrl: "./rol.component.html",
  styleUrls: ["./rol.component.css"],
})
export class RolComponent implements OnInit {
  cantidad: number = 0;
  dataSource: MatTableDataSource<Rol>;
  displayedColumns: string[] = ["idRol", "nombre", "descripcion", "acciones"];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private rol_service: RolService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.rol_service.rolCambio.subscribe((data) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.rol_service.mensajeCambio.subscribe((data) => {
      this.snackBar.open(data, "AVISO", {
        duration: 2000,
      });
    });
    this.rol_service.listarPageable(0, 10).subscribe((data) => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  eliminar(rol: Rol) {
    this.rol_service
      .eliminar(rol.idRol)
      .pipe(
        switchMap(() => {
          return this.rol_service.listar();
        })
      )
      .subscribe((data) => {
        this.rol_service.rolCambio.next(data);
        this.rol_service.mensajeCambio.next("SE ELIMINO");
      });
  }

  filter(x: string) {
    this.dataSource.filter = x.trim().toLowerCase();
  }
  mostrarMas(e: any) {
    this.rol_service
      .listarPageable(e.pageIndex, e.pageSize)
      .subscribe((data) => {
        this.cantidad = data.totalElements;
        this.dataSource = new MatTableDataSource(data.content);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }
}
