import { switchMap } from "rxjs/operators";
import { Rol } from "./../../../_model/rol";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { RolService } from "../../../_service/rol.service";
import { Component, OnInit} from "@angular/core";

@Component({
  selector: "app-rol-edicion",
  templateUrl: "./rol-edicion.component.html",
  styleUrls: ["./rol-edicion.component.css"],
})
export class RolEdicionComponent implements OnInit {
  id: number;
  rol: Rol;
  edicion: boolean;
  form: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private rol_service: RolService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initForm(false);
    this.route.params.subscribe((params: Params) => {
      this.id = params["id"];
      this.edicion = this.id != null;
      if (this.edicion) {
        this.rol_service.listarPorId(this.id).subscribe((data) => {
          this.initForm(true, data);
        });
      } else {
        this.initForm(false);
      }
    });
  }
  get f() {
    return this.form.controls;
  }
  initForm(edita: boolean, rol?: Rol) {
    this.form = new FormGroup({
      idRol: new FormControl(edita ? rol.idRol : 0),
      nombre: new FormControl(edita ? rol.nombre : "", Validators.required),
      descripcion: new FormControl(
        edita ? rol.descripcion : "",
        Validators.required
      ),
    });
  }
  operar() {
    let rol = new Rol();
    rol.idRol = this.form.value["idRol"];
    rol.nombre = this.form.value["nombre"];
    rol.descripcion = this.form.value["descripcion"];
    if (rol.idRol > 0) {
      this.rol_service
        .modificar(rol)
        .pipe(
          switchMap(() => {
            return this.rol_service.listar();
          })
        )
        .subscribe((data) => {
          this.rol_service.rolCambio.next(data);
          this.rol_service.mensajeCambio.next("SE MODIFICÓ");
        });
    } else {
      this.rol_service
        .registrar(rol)
        .pipe(
          switchMap(() => {
            return this.rol_service.listar();
          })
        )
        .subscribe((data) => {
          this.rol_service.rolCambio.next(data);
          this.rol_service.mensajeCambio.next("SE REGISTRO");
        });
    }
    this.router.navigate(['rol']);
  }
}
