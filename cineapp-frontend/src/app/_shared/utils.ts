import { Rol } from "./../_model/rol";
export class Util {
  static getArrayRol(roles: Rol[], id: number) {
    let array = [];
    roles.forEach((rol) => {
      if (id == rol.idRol) {
        array.push(rol);
      }
    });
    return array;
  }
  static idRol(rol: Rol[]) {
    let idRol: number;
    rol.forEach((r) => {
      idRol = r.idRol;
    });
    return idRol;
  }
}
