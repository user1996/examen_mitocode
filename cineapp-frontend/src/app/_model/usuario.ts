import { Rol } from './rol';
import { Cliente } from './cliente';
export class Usuario {
    idUsuario: number;
    cliente: Cliente;
    nombre: string;
    clave: string;
    claveoculta:string;
    estado: boolean;
    roles:Rol[];
}