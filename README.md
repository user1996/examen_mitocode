# Examen final del curos Java Full Stack

Scripts necesarios para que funcione el sistema.

## Video de la explicacion

[![Watch the video](https://imagekit.androidphoria.com/wp-content/uploads/Video-youtube2.jpg)](https://drive.google.com/open?id=1ngT0-zzMzyEZi8HSX8OkIzBcztVqngDE)
## Base de datos MySql "cineapp_examen"

### Tablas para la sesion.
```
CREATE TABLE oauth_refresh_token (
  token_id varchar(256) DEFAULT NULL,
  token longblob DEFAULT NULL,
  authentication longblob DEFAULT NULL
);

CREATE TABLE oauth_access_token (
  token_id varchar(256) DEFAULT NULL,
  token longblob DEFAULT NULL,
  authentication_id varchar(256) DEFAULT NULL,
  user_name varchar(256) DEFAULT NULL,
  client_id varchar(256) DEFAULT NULL,
  authentication longblob DEFAULT NULL,
  refresh_token varchar(256) DEFAULT NULL
);
```
### Tabla cliente
```
ALTER TABLE cliente
MODIFY  foto longblob; 
INSERT INTO cliente VALUES
(1, 'CODE CODE', '23215450', '1991-01-21', NULL, 'MITO'),
(2, 'CASTILLO CHALQUE', '3697852', '1996-03-12', NULL, 'JOSE'),
(3, 'PEREZ ESTRADA', '75624412', '1996-03-12', NULL, 'CARLOS'),
(5, 'APELLIDO PERSONA', '03697826', '1995-06-13', NULL, 'PEDRO'),
(4, 'DELGADO MORENO', '07440945', '1991-01-21', NULL, 'JULIO'),
(14, 'PEREZ AGUIRRE', '02515478', '1997-06-09', NULL, 'JUAN ');
```
### Tabla usuario
```
INSERT INTO usuario VALUES
(1, '$2a$10$loV3cc1fDbIAltgQs/RbdO6nt.Dy4aewPhePsdiUBCI6I/mGrcUum', True, 'mitocode'),
(2, '$2a$10$5xd6aFUjWTPDHHg/AYL0reggebuTz8p2TfZinUAFmIYRYpiDVgop6', True, 'admin'),
(3, '$2a$10$BYzID0dVmdtbz.kkDcBhbejYXhGZ.x0/V12ikC5ILlI1C9yBoHZRe', True, 'cperez'),
(4, '$2a$10$PBaUj.Spnqj04FZ0CUfZjejbW7WgU0mYr37Tf8/7zwtpquGX8ax5K', True, 'papurri'),
(5, '$2a$10$DOh3z0QWQHwYKE2X7V61qOV5uAGx9p2TDEUPBWn9vWaVqU8uQWPF2', True, 'papellido'),
(14, '$2a$10$2txsSvp6hHmgQ3Nocpmqg.BiO0bceKQtfV172ms232MWEE8gem9Fe', True, 'tapir');
```
### Tabla rol
```
INSERT INTO rol VALUES
(1, 'Administrador', 'ADMIN'),
(2, 'Usuario', 'USER'),
(7, 'Base de datos', 'DBA');
```
### Tabla usuario_rol
```
INSERT INTO usuario_rol VALUES
(1, 2),
(2, 1),
(3, 2),
(5, 2),
(4, 1),
(14, 2);
```
### Tabla genero
```
INSERT INTO genero VALUES
(1, 'AVENTURA'),
(2, 'DRAMA'),
(3, 'MUSICAL'),
(4, 'FICCIÓN'),
(5, 'OESTE'),
(6, 'SUSPENSO'),
(7, 'INFANTIL'),
(8, 'ANIMACIÓN'),
(9, 'BÉLICO'),
(10, 'HISTÓRICO'),
(11, 'ADULTO'),
(12, 'DEPORTIVO'),
(13, 'ANTIGUAS'),
(14, 'BLANCO/NEGRO'),
(15, 'MISTERIO'),
(16, 'FANTASIA'),
(17, 'RELIGION'),
(18, 'EPICO'),
(19, 'POLICIACO'),
(20, 'COMEDIA ROMANTICA'),
(21, 'TERROR INFANTIL'),
(22, 'SUBREALISTA'),
(23, 'CASERO'),
(24, 'AFICIONADO');
```
### Tabla pelicula
```
INSERT INTO pelicula VALUES
(1, 120, '2020-04-04', 'Historias de miedo para contar en la oscuridad', 'La familia Bellows y su sombra ha aumentado en Mill Valley, ya que las historias de terror de la joven Sarah empiezan a cobrar realidad. Un grupo de adolescentes debe resolver el misterio que rodea a una serie de repentinas y macabras muertes que suceden en su pueblo.', 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/historias-de-miedo-para-contar-en-la-oscuridad-pelicula-poster-1559633183.jpg?resize=320:*', 21),
(2, 120, '2020-04-09', 'Annabelle 3: vuelve a casa', 'Ed y Lorraine Warren tratan de encerrar bajo llave a Annabelle, una muñeca poseída. Como demonólogos la colocaran en una vitrina bendecida como medida de seguridad, sin embargo, una noche terrorífica Annabelle despertará a otros espíritus malignos que se encuentran en la habitación y que tendrán una…', 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTgVkiMeIsXxY_gmdsMn-0IwSgj5Ywn2rq5aGM8DRu2eoMtbfLA&usqp=CAU', 21),
(3, 90, '2020-04-24', 'Exterminio 1', 'En el centro de investigación de primates de Cambridge una incursión de activistas a favor de los derechos de los animales accede a un laboratorio de investigación científica, con la intención de liberar a un grupo de chimpancés que están siendo utilizados en diversos experimentos secretos. ', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQEFJPZ3ivLawdpyREBWww0pU_DZ_Z0qqCkzClrLfuaSabS_L-n9Q&s', 6),
(4, 110, '2020-04-24', 'Exterminio 2', 'Seis meses después de la epidemia original, el virus de la rabia ha aniquilado a la población de las Islas Británicas. No obstante, el ejército estadounidense declara que el peligro ha pasado, y los soldados americanos llegan para restaurar el orden y empezar la reconstrucción. Refugiados regresan a', 'https://vignette.wikia.nocookie.net/doblaje/images/f/f0/Exterminio2_poster.jpg/revision/latest?cb=20200203010915&path-prefix=es', 6);

```
### Tabla menu
```
INSERT INTO menu VALUES
(1, 'search', 'Consultas', '/consulta'),
(2, 'bookmarks', 'Venta de Entradas', '/venta'),
(3, 'account_circle', 'Clientes', '/cliente'),
(4, 'local_movies', 'Peliculas', '/pelicula'),
(5, 'fastfood', 'Comidas', '/comida'),
(6, 'view_agenda', 'Géneros', '/genero'),
(7, 'assessment', 'Reportes', '/reporte'),
(8, 'build', 'Configuraciones', '/configuracion'),
(9, 'account_box', 'Perfil', '/perfil'),
(10, 'accessibility_new', 'Roles', '/rol');
```
### Tabla menu_rol
```
INSERT INTO menu_rol VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(9, 2),
(9, 1),
(10, 1),
(1, 3),
(3, 3);
```
### Tabla config
```
INSERT INTO config VALUES
(1, 'ENT', '12');
```
## Tabla comida
```
ALTER TABLE comida
MODIFY  foto longblob;   
INSERT INTO comida VALUES
(1, NULL, 'LOMO SALTADO', 15.30);
```
### Tabla venta
```
INSERT INTO venta VALUES
(1, 2, '2020-04-10 11:23:36', 39.40, 2, 1),
(2, 2, '2020-04-10 11:29:04', 35.12, 2, 1),
(3, 2, '2020-04-11 11:31:10', 25.36, 2, 1),
(4, 2, '2020-04-11 11:31:48', 39.30, 4, 1),
(5, 7, '2020-04-11 11:33:08', 99.30, 4, 1),
(6, 8, '2020-04-12 11:33:27', 80.70, 6, 1),
(7, 2, '2020-04-25 18:58:04', 39.30, 3, 2);
```
### Tabla venta_comida.
```
INSERT INTO venta_comida VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 7);
```
### Tabla detalle_venta
```
INSERT INTO detalle_venta VALUES
(1, '25', 1),
(2, '26', 1),
(3, '25', 2),
(4, '26', 2),
(5, '25', 3),
(6, '26', 3),
(7, '57', 4),
(8, '45', 4),
(9, '64', 5),
(10, '65', 5),
(11, '66', 5),
(12, '67', 5),
(13, '68', 5),
(14, '69', 5),
(15, '70', 5),
(16, '64', 6),
(17, '65', 6),
(18, '66', 6),
(19, '67', 6),
(20, '68', 6),
(21, '69', 6),
(22, '79', 6),
(23, '73', 6),
(24, '44', 7),
(25, '46', 7);
```
### Para Visualizar los graficos
```
CREATE VIEW fn_listarresumen
AS
SELECT
  COUNT(`tv`.`id_venta`) AS `cantidad`,
  DATE_FORMAT(`tv`.`fecha`, '%d/%m/%Y') AS `fecha`
FROM `venta` `tv`
GROUP BY DATE_FORMAT(`tv`.`fecha`, '%d/%m/%Y')
ORDER BY DATE_FORMAT(`tv`.`fecha`, '%d/%m/%Y');
```

